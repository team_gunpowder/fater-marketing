SELECT DISTINCT
    b.SubscriberKey,
    b.BounceCategory,
    s.EmailAddress,
    users.Phone
FROM
    [_bounce] b
JOIN [_Subscribers] s
    ON s.SubscriberKey = b.SubscriberKey
JOIN [All User Pampers] users
    ON users.PersonEmail = s.EmailAddress
WHERE 
    users.Phone != '' AND users.Phone != '*****'
    AND b.BOUNCECATEGORY ='Hard bounce'