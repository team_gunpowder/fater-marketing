SELECT DISTINCT
   users.Id,
   users.PersonEmail,
   users.PersonContactId,
   users.LastName,
   users.Phone,
   users.FirstName,
   users.ExtId__c,
   users.Club__pc,
   users.Data_Club__pc,
   users.Data_MKT_Email__pc,
   users.MKT_Email__pc,
   users.Data_Profilazione__pc,
   users.Profilazione__pc,
   users.PresumedBirthDate__pc,
   users.MonthBaby__pc,
   users.AppVerNumber__pc as AppVerNumber,
   users.HasOptedOutOfEmail,
   users.Status,
   users.PrivacyPersonalData__pc,
   users.Region__pc,
   users.CurrentWeek__pc,
   users.PrivacyPersonalDate__pc,
  '176' as MissionSchemaId
   
FROM
   [All Coccole Pampers Rewardind 2021] users
   

