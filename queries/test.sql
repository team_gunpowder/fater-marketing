SELECT DISTINCT
   users.Id,
   users.PersonEmail,
   users.PersonContactId,
   users.LastName,
   users.Phone,
   users.FirstName,
   users.ExtId__c,
   users.Club__pc,
   users.Data_Club__pc,
   users.Data_MKT_Email__pc,
   users.MKT_Email__pc,
   users.Data_Profilazione__pc,
   users.Profilazione__pc,
   users.PresumedBirthDate__pc,
   users.MonthBaby__pc,
   users.AppVerNumber__pc as AppVerNumber,
   habits.AppDownloaded__c
FROM
   [All User Pampers] users
JOIN
   [Abitudini di acquisto] habits 
      ON users.Id = habits.Account__c 
WHERE
   users.PersonEmail IN 
   (
      'm.debaptistis@gp-start.it',
      'm.disilvio@gp-start.it',
      'f.ioannone@gp-start.it' 
   )