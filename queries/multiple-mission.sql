 SELECT
    users.PersonEmail,
    users.LastName,
    users.FirstName,
    users.PersonContactId,
    users.ExtId__c,
    users.Id,
    users.MonthBaby__pc,
    users.Club__pc,
    users.Data_Club__pc,
    users.Data_MKT_Email__pc,
    users.MKT_Email__pc,
    users.Data_Profilazione__pc,
    users.Profilazione__pc,
    users.CurrentWeekNL__pc,
    CONVERT(DATE,PresumedBirthDate__pc) as PresumedBirthDate__pc,
    users.PersonHasOptedOutOfEmail,
    users.CurrentWeek__pc,
   CONVERT(DATE,GETDATE()) as Today,
   'IT' as Zone,
CASE
    WHEN users.CurrentWeek__pc = 2 THEN '100'
    WHEN users.CurrentWeek__pc = 4 THEN '101'
    WHEN users.CurrentWeek__pc = 6 THEN '102'
    WHEN users.CurrentWeek__pc = 8 THEN '103'
    WHEN users.CurrentWeek__pc = 10 THEN '104'
    WHEN users.CurrentWeek__pc = 12 THEN '105'
    WHEN users.CurrentWeek__pc = 14 THEN '106'
    WHEN users.CurrentWeek__pc = 16 THEN '107'
    WHEN users.CurrentWeek__pc = 18 THEN '108'
    WHEN users.CurrentWeek__pc = 20 THEN '109'
    WHEN users.CurrentWeek__pc = 22 THEN '110'
    WHEN users.CurrentWeek__pc = 24 THEN '111'
    WHEN users.CurrentWeek__pc = 26 THEN '112'
    WHEN users.CurrentWeek__pc = 28 THEN '113'
    WHEN users.CurrentWeek__pc = 30 THEN '114'
    WHEN users.CurrentWeek__pc = 32 THEN '115'
    WHEN users.CurrentWeek__pc = 34 THEN '116'
    WHEN users.CurrentWeek__pc = 36 THEN '117'
    WHEN users.CurrentWeek__pc = 38 THEN '118'
    WHEN users.CurrentWeek__pc = 40 THEN '119'
END AS MissionSchemaId
FROM
    [All Coccole Pampers Rewardind 2021] users
WHERE 
   CONVERT(DATE,PresumedBirthDate__pc) > CONVERT(DATE,GETDATE()) AND
   CurrentWeek__pc > 0 
   AND CurrentWeek__pc <= 40 
   AND PresumedBirthDate__pc >= DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0) 
   AND (DATEDIFF(day, GETDATE(), DATEADD(day, 1,PresumedBirthDate__pc)) % 7 ) = 0 AND
   ( users.CurrentWeek__pc = 2 
    OR users.CurrentWeek__pc = 4 
    OR users.CurrentWeek__pc = 6 
    OR users.CurrentWeek__pc = 8 
    OR users.CurrentWeek__pc = 10 
    OR users.CurrentWeek__pc = 12 
    OR users.CurrentWeek__pc = 14 
    OR users.CurrentWeek__pc = 16 
    OR users.CurrentWeek__pc = 18 
    OR users.CurrentWeek__pc = 20 
    OR users.CurrentWeek__pc = 22 
    OR users.CurrentWeek__pc = 24 
    OR users.CurrentWeek__pc = 26 
    OR users.CurrentWeek__pc = 28 
    OR users.CurrentWeek__pc = 30 
    OR users.CurrentWeek__pc = 32 
    OR users.CurrentWeek__pc = 34 
    OR users.CurrentWeek__pc = 36 
    OR users.CurrentWeek__pc = 38 
    OR users.CurrentWeek__pc = 40 )



   ----------------------------------- QUIZ GRAVIDANZA -------------------------------------------------
   SELECT
    users.PersonEmail,
    users.LastName,
    users.FirstName,
    users.PersonContactId,
    users.ExtId__c,
    users.Id,
    users.MonthBaby__pc,
    users.Club__pc,
    users.Data_Club__pc,
    users.Data_MKT_Email__pc,
    users.MKT_Email__pc,
    users.Data_Profilazione__pc,
    users.Profilazione__pc,
    users.CurrentWeekNL__pc,
    CONVERT(DATE,PresumedBirthDate__pc) as PresumedBirthDate__pc,
    users.PersonHasOptedOutOfEmail,
    users.CurrentWeek__pc,
   CONVERT(DATE,GETDATE()) as Today,
   'IT' as Zone,
CASE
    WHEN users.MonthBaby__pc = 2 THEN '100'
    WHEN users.MonthBaby__pc = 4 THEN '101'
    WHEN users.MonthBaby__pc = 6 THEN '102'
    WHEN users.MonthBaby__pc = 8 THEN '103'
    WHEN users.MonthBaby__pc = 10 THEN '104'
    WHEN users.MonthBaby__pc = 12 THEN '105'
    WHEN users.MonthBaby__pc = 14 THEN '106'
    WHEN users.MonthBaby__pc = 16 THEN '107'
    WHEN users.MonthBaby__pc = 18 THEN '108'
    WHEN users.MonthBaby__pc = 20 THEN '109'
    WHEN users.MonthBaby__pc = 22 THEN '110'
    WHEN users.MonthBaby__pc = 24 THEN '111'
    WHEN users.MonthBaby__pc = 26 THEN '112'
    WHEN users.MonthBaby__pc = 28 THEN '113'
    WHEN users.MonthBaby__pc = 30 THEN '114'
    WHEN users.MonthBaby__pc = 32 THEN '115'
    WHEN users.MonthBaby__pc = 34 THEN '116'
    WHEN users.MonthBaby__pc = 36 THEN '117'
    WHEN users.MonthBaby__pc = 38 THEN '118'
    WHEN users.MonthBaby__pc = 40 THEN '119'
END AS MissionSchemaId
FROM
    [All Coccole Pampers Rewardind 2021] users
WHERE 
   CONVERT(DATE,PresumedBirthDate__pc) < CONVERT(DATE,GETDATE()) AND
   MonthBaby__pc > 0 
   AND MonthBaby__pc <= 24 AND
  DATEADD(MONTH, MonthBaby__pc, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE()) AND
   ( users.CurrentWeek__pc = 2 
    OR users.CurrentWeek__pc = 4 
    OR users.CurrentWeek__pc = 6 
    OR users.CurrentWeek__pc = 8 
    OR users.CurrentWeek__pc = 10 
    OR users.CurrentWeek__pc = 12 
    OR users.CurrentWeek__pc = 14 
    OR users.CurrentWeek__pc = 16 
    OR users.CurrentWeek__pc = 18 
    OR users.CurrentWeek__pc = 20 
    OR users.CurrentWeek__pc = 22 
    OR users.CurrentWeek__pc = 24 
    OR users.CurrentWeek__pc = 26 
    OR users.CurrentWeek__pc = 28 
    OR users.CurrentWeek__pc = 30 
    OR users.CurrentWeek__pc = 32 
    OR users.CurrentWeek__pc = 34 
    OR users.CurrentWeek__pc = 36 
    OR users.CurrentWeek__pc = 38 
    OR users.CurrentWeek__pc = 40 )


----------------- PUERICULTURA -----------------

 SELECT
    users.PersonEmail,
    users.LastName,
    users.FirstName,
    users.PersonContactId,
    users.ExtId__c,
    users.Id,
    users.MonthBaby__pc,
    users.Club__pc,
    users.Data_Club__pc,
    users.Data_MKT_Email__pc,
    users.MKT_Email__pc,
    users.Data_Profilazione__pc,
    users.Profilazione__pc,
    users.CurrentWeekNL__pc,
    CONVERT(DATE,PresumedBirthDate__pc) as PresumedBirthDate__pc,
    users.PersonHasOptedOutOfEmail,
    users.CurrentWeek__pc,
    CONVERT(DATE,GETDATE()) as Today,
   'IT' as Zone,
CASE
    WHEN DATEADD(DAY, 15, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '120'
    WHEN DATEADD(MONTH, 2, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '122'
    WHEN DATEADD(MONTH, 3, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '125'
    WHEN DATEADD(MONTH, 4, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '126'
    WHEN DATEADD(MONTH, 5, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '127'
    WHEN DATEADD(MONTH, 6, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '129'
    WHEN DATEADD(MONTH, 7, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '130'
    WHEN DATEADD(MONTH, 7, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '130'
    WHEN DATEADD(DAY, 15, DATEADD(MONTH, 7, CONVERT(DATE,PresumedBirthDate__pc))) = CONVERT(DATE,GETDATE())  THEN '132'
    WHEN DATEADD(MONTH, 8, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '133'
    WHEN DATEADD(DAY, 15, DATEADD(MONTH, 8, CONVERT(DATE,PresumedBirthDate__pc))) = CONVERT(DATE,GETDATE())  THEN '164'
    WHEN DATEADD(DAY, 15, DATEADD(MONTH, 9, CONVERT(DATE,PresumedBirthDate__pc))) = CONVERT(DATE,GETDATE())  THEN '137'
    WHEN DATEADD(MONTH, 10, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '138'
    WHEN DATEADD(DAY, 15, DATEADD(MONTH, 10, CONVERT(DATE,PresumedBirthDate__pc))) = CONVERT(DATE,GETDATE())  THEN '139'
    WHEN DATEADD(MONTH, 11, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '140'
    WHEN DATEADD(MONTH, 12, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '141'
    WHEN DATEADD(MONTH, 13, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '165'
    WHEN DATEADD(MONTH, 14, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '145'
    WHEN DATEADD(MONTH, 15, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '146'
    WHEN DATEADD(MONTH, 16, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '147'
    WHEN DATEADD(MONTH, 17, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '148'
    WHEN DATEADD(MONTH, 18, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '149'
    WHEN DATEADD(MONTH, 19, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '150'
    WHEN DATEADD(MONTH, 20, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '151'
    WHEN DATEADD(MONTH, 21, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '152'
    WHEN DATEADD(MONTH, 22, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '153'
    WHEN DATEADD(MONTH, 23, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '154'
   
END AS MissionSchemaId
FROM
    [All Coccole Pampers Rewardind 2021] users
WHERE
   CONVERT(DATE, PresumedBirthDate__pc) <= CONVERT(DATE, GETDATE()) 
   AND MonthBaby__pc >= 0 
   AND MonthBaby__pc <= 24 
   AND 
   (
      DATEADD(DAY, 15, CONVERT(DATE, PresumedBirthDate__pc)) = CONVERT(DATE, GETDATE()) 
        OR DATEADD(MONTH, 2, CONVERT(DATE, PresumedBirthDate__pc)) = CONVERT(DATE, GETDATE()) 
        OR DATEADD(MONTH, 3, CONVERT(DATE, PresumedBirthDate__pc)) = CONVERT(DATE, GETDATE()) 
        OR DATEADD(MONTH, 5, CONVERT(DATE, PresumedBirthDate__pc)) = CONVERT(DATE, GETDATE()) 
        OR DATEADD(MONTH, 6, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())
        OR DATEADD(MONTH, 7, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())
        OR DATEADD(MONTH, 7, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE()) 
        OR DATEADD(DAY, 15, DATEADD(MONTH, 7, CONVERT(DATE,PresumedBirthDate__pc))) = CONVERT(DATE,GETDATE())
        OR DATEADD(MONTH, 8, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE()) 
        OR DATEADD(MONTH, 8, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())
        OR DATEADD(DAY, 15, DATEADD(MONTH, 8, CONVERT(DATE,PresumedBirthDate__pc))) = CONVERT(DATE,GETDATE())
        OR DATEADD(DAY, 15, DATEADD(MONTH, 9, CONVERT(DATE,PresumedBirthDate__pc))) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 10, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(DAY, 15, DATEADD(MONTH, 10, CONVERT(DATE,PresumedBirthDate__pc))) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 11, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 12, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE()) 
        OR DATEADD(MONTH, 13, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 14, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 15, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 16, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 17, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 18, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 19, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 20, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 21, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 22, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 23, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
   )




----------------- PUERICULTURA V2 -----------------

 SELECT
    users.PersonEmail,
    users.LastName,
    users.FirstName,
    users.PersonContactId,
    users.ExtId__c,
    users.Id,
    users.MonthBaby__pc,
    users.Club__pc,
    users.Data_Club__pc,
    users.Data_MKT_Email__pc,
    users.MKT_Email__pc,
    users.Data_Profilazione__pc,
    users.Profilazione__pc,
    users.CurrentWeekNL__pc,
    CONVERT(DATE,PresumedBirthDate__pc) as PresumedBirthDate__pc,
    users.PersonHasOptedOutOfEmail,
    users.CurrentWeek__pc,
    CONVERT(DATE,GETDATE()) as Today,
   'IT' as Zone,
CASE
    WHEN DATEADD(DAY, 15, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '120'
    WHEN DATEADD(MONTH, 2, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '122'
    WHEN DATEADD(MONTH, 3, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '125'
    WHEN DATEADD(MONTH, 4, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '126'


    WHEN DATEADD(MONTH, 7, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '130'
    WHEN DATEADD(MONTH, 7, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '130'

    WHEN DATEADD(MONTH, 8, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '133'


    WHEN DATEADD(MONTH, 10, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '138'

    WHEN DATEADD(MONTH, 11, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '140'

    WHEN DATEADD(MONTH, 14, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '145'
    WHEN DATEADD(MONTH, 15, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '146'
    WHEN DATEADD(MONTH, 16, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '147'
    WHEN DATEADD(MONTH, 17, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '148'
    WHEN DATEADD(MONTH, 18, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '149'
    WHEN DATEADD(MONTH, 19, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '150'
    WHEN DATEADD(MONTH, 20, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '151'
    WHEN DATEADD(MONTH, 21, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '152'
    WHEN DATEADD(MONTH, 22, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '153'
    WHEN DATEADD(MONTH, 23, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  THEN '154'
   
END AS MissionSchemaId
FROM
    [All Coccole Pampers Rewardind 2021] users
WHERE
   CONVERT(DATE, PresumedBirthDate__pc) <= CONVERT(DATE, GETDATE()) 
   AND MonthBaby__pc >= 0 
   AND MonthBaby__pc <= 24 
   AND 
   (
      DATEADD(DAY, 15, CONVERT(DATE, PresumedBirthDate__pc)) = CONVERT(DATE, GETDATE()) 
        OR DATEADD(MONTH, 2, CONVERT(DATE, PresumedBirthDate__pc)) = CONVERT(DATE, GETDATE()) 
        OR DATEADD(MONTH, 3, CONVERT(DATE, PresumedBirthDate__pc)) = CONVERT(DATE, GETDATE()) 
  
        OR DATEADD(MONTH, 7, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE()) 
        OR DATEADD(MONTH, 8, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE()) 



        OR DATEADD(MONTH, 10, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
   
        OR DATEADD(MONTH, 11, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  

        OR DATEADD(MONTH, 13, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 14, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  

        OR DATEADD(MONTH, 16, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 17, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 18, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 19, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 20, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 21, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 22, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
        OR DATEADD(MONTH, 23, CONVERT(DATE,PresumedBirthDate__pc)) = CONVERT(DATE,GETDATE())  
   )