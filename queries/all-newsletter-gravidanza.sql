SELECT
   Id,
   PersonEmail as Email,
   LastName,
   FirstName,
   Name,
   Phone,
   PresumedBirthDateNL__pc AS PresumedBirthDate__c,
   CurrentQuarterNL__pc AS CurrentQuarter__c,
   CurrentWeekNL__pc AS CurrentWeek__c,
   Club__pc AS Club__c,
   Gravidanza__pc AS Gravidanza__c,
   MKT_Email__pc AS MKT_Email__c,
   Profilazione__pc AS Profilazione__c,
   PersonHasOptedOutOfEmail as HasOptedOutOfEmail,
   PersonContactId as PersonContactId,
   GravidanzaDate__pc as GravidanzaDate__c,
   RegulationAcceptance2__pc as RegulationAcceptance2__c,
   RegulationAcceptance__pc as RegulationAcceptance__c,
   DATEDIFF(day, GETDATE(), PresumedBirthDateNL__pc) AS daysPresumedBirthDate 
FROM
   Account_Salesforce 
WHERE
   Gravidanza__pc = 1 