SELECT 
    Codice_Database__c,
    Reward_Id__c, 
    Reward_Name__c ,
    Stato_Richiesta__c,
    Data_Richiesta_Utente__c, 
    Data_Stato_Richiesta__c,
    Count(Codice_Database__c) AS CountCodiceDb
FROM 
    Tracking_Award__c_Salesforce
WHERE 
    Stato_Richiesta__c  = 'In Consegna'
Group by 
    Codice_Database__c,
    Reward_Id__c, 
    Reward_Name__c,
    Stato_Richiesta__c,
    Data_Richiesta_Utente__c,
    Data_Stato_Richiesta__c
Having CountCodiceDb <= 2


SELECT  UsersId__c,EAN__c,
count(Caricamenti__c) AS TotaleCaricamenti,
 SUM(Punti__c) AS PuntiTotali,
 MAX(DataGiocata__c) AS UltimaDataGiocata 
 FROM ValidCodeCesar 
 GROUP BY EAN__c,UsersId__c


 SELECT
    tracking.[Tracking_Award__c:Tipologia_Spedizione__c] as tipologiaSpedizione,
    tracking.[Tracking_Award__c:Stato_Richiesta__c] as statoRichiesta,
    tracking.[Tracking_Award__c:Account__r:ExtId__pc] as Account_ExtId__pc,
    tracking.[Tracking_Award__c:ExtId__c] as ExtId__c
    
FROM
    [Tracking Awards Event - 2021-04-14T114327122] tracking



    SELECT
    tracking.[Tracking_Award__c:Tipologia_Spedizione__c] as tipologiaSpedizione,
    tracking.[Tracking_Award__c:Stato_Richiesta__c] as statoRichiesta,
    tracking.[Tracking_Award__c:Account__r:ExtId__pc] as Account_ExtId__pc,
    tracking.[Tracking_Award__c:ExtId__c] as ExtId__c
    
FROM
    [Tracking Awards Event - 2021-04-14T114327122] tracking
WHERE
    tracking.[Tracking_Award__c:ExtId__c] NOT IN 
        (SELECT ExtId__c FROM "Tracking_Award__c_Salesforce"
        WHERE 
            Stato_Richiesta__c = 'In consegna' OR
            Stato_Richiesta__c = 'Consegnato' 
        )