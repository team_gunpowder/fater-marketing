<script type = "javascript" runat = "server">
Platform.Load("core", "1.1.5");

/**
 * GLOBAL VARIABLE
 */
var scriptName = "Eliminazione Utenti Anonimizzati";
/**
 * FUNCTIONS
 */

/**
* @description Get Items from DataExtension
*/
function getItemsFromDataExtension() {
    /*Defining retrieve request for DataExtension*/
    var items = [];
    var rr = Platform.Function.CreateObject("RetrieveRequest");

    Platform.Function.AddObjectArrayItem(rr, "Properties", "SubscriberId");
    Platform.Function.AddObjectArrayItem(rr, "Properties", "SubscriberKey");

    Platform.Function.SetObjectProperty(rr, 'ObjectType', 'DataExtensionObject[D9D81CF9-0AEF-4E2C-A82C-61545846E178]');
    do {
        /*Effectively retrieving data extension records*/
        Write('*** Getting Data *** <br><br>');
        var StatusAndRequestID = [0, 0];
        var status = StatusAndRequestID[0];
        var requestId = StatusAndRequestID[1];
        items = items.concat(Platform.Function.InvokeRetrieve(rr, StatusAndRequestID));

    } while (status === "MoreDataAvailable");
    return items;

}

/**
 * @description Auth function for Marketing cloud
 */
function authMarketingCloud(endpoint, contentType, auth) {
    var response = {
        accessToken: "",
        restInstanceUrl: ""
    };
    try {
        Write('*** Getting access Token *** <br><br>');
        result = HTTP.Post(endpoint, contentType, Stringify(auth));
        mcResponse = Platform.Function.ParseJSON(result.Response[0]);
        response.accessToken = mcResponse["access_token"];
        response.restInstanceUrl = mcResponse["rest_instance_url"];


    } catch (ex) {
        Write('ERROR ACCESS TOKEN MARKETING CLOUD');
        Write(ex);
        Platform.Function.InsertDE('DE to Save Errors', ['Timestamp', 'Error Text', 'Automation Name'], [Now(), 'ERROR ACCESS TOKEN MARKETING CLOUD-->' + ex, scriptName]);
        return;
    }
    return response;
}

/**
 * @description https://developer.salesforce.com/docs/atlas.en-us.noversion.mc-apis.meta/mc-apis/DeleteByContactKeys.htm
 */
function deleteSubscribers(endPoint, contentType, token, subscribers) {
    Write("*** Deleting elements *** <br><br>");
    payload = {
        "ContactTypeId": 0,
        "values": subscribers,
        "DeleteOperationType": "ContactAndAttributes"
    };
    authHeader = ["Authorization"];
    tokenHeader = ["Bearer " + token];
    try {
        result = HTTP.Post(endPoint, contentType, Stringify(payload), authHeader, tokenHeader);

    } catch (error) {
        Write('configDeleteContact error: <br><br> ' + error);

    }
    return result;

}

/**
 * @description https://developer.salesforce.com/docs/atlas.en-us.noversion.mc-apis.meta/mc-apis/ContactsDeleteConfigStatus.htm
 */
function configDeleteContact(endPoint, contentType, token) {
    Write('*** Config delete contacts *** <br><br>');
    payload = {
        "items": [{
            "settingKey": "SuppressionRestoreUntilDays",
            "value": "-1"
        }
        ]
    };
    authHeader = ["Authorization"];
    tokenHeader = ["Bearer " + token];
    try {
        result = HTTP.Post(endPoint, contentType, Stringify(payload), authHeader, tokenHeader);

    } catch (error) {
        Write('configDeleteContact error: <br><br> ' + error);

    }
    Write("configDeleteContact: <br><br>" + Stringify(result) + "<br><br>");

    return result;
}


function main() {

    var subscribersToRemove = [];
    var urlAuthMarketingCloud = "https://mc4v97cd5m8pm-1s6jb6569nhn24.auth.marketingcloudapis.com/v2/token";

    var endPoints = {
        "contacts": {
            "deleteContacts": "contacts/v1/contacts/actions/delete?type=keys",
            "deleteContactsSettings": "contacts/v1/contacts/actions/delete/configSettings"
        }

    };
    var contentType = {
        "application/json": "application/json"
    };
    var authMc = {
        client_id: "xqod7qbfa5lapkvnoax73imk",
        client_secret: "DpaWXR4M7jWXo46ByC8KOW5x",
        grant_type: "client_credentials"
    };


    var responseAuth            = authMarketingCloud(urlAuthMarketingCloud, contentType["application/json"], authMc);
    var responseConfigDelete    = configDeleteContact(responseAuth.restInstanceUrl + endPoints.contacts.deleteContactsSettings, contentType["application/json"], responseAuth.accessToken);
    var itemsToDelete           = getItemsFromDataExtension();

    if (itemsToDelete != null) {
        for (var i in itemsToDelete) {
            // Write("item to delete: <br><br> " + Stringify(itemsToDelete[i]) + "<br><br>");
            subscribersToRemove.push(itemsToDelete[i]['Properties'][1]['Value']);
        }
    }
    Write("items to delete: <br><br>" + Stringify(subscribersToRemove) + "<br><br>");
    // Write ("url for deleting: <br><br>"+ responseAuth.restInstanceUrl  +"<br><br>");
    // Write ("ACTIONS: <br><br>"+ endPoints.contacts.deleteContacts  +"<br><br>");
    var responseSubscribersDeleted = deleteSubscribers(responseAuth.restInstanceUrl + endPoints.contacts.deleteContacts, contentType["application/json"], responseAuth.accessToken, subscribersToRemove);
    Write("Response subscribers deleted: <br><br>" + Stringify(responseSubscribersDeleted) + "<br><br>");
}

/**
 * MAIN
 */

main();





</script>